﻿#if CRAZY_TWEEN_INCLUDED
using CrazyRam.Core.Tween;
#endif
using UnityEngine;

namespace CrazyRam.Core.Time
{
    public class CrazyTimeManager : Singleton<CrazyTimeManager>
    {
        [SerializeField]
        private float _multipler;

#if CRAZY_TWEEN_INCLUDED
        private ITween<float> _timeScaleTween;
#endif

        #region Variables

        private float _lastRealTime;

        private float _lastPhysicsTime;

        #endregion

        #region Getters

        public static float Time { get; private set; }

        public static float UnscaledTime { get; private set; }

        public static float PhysicalTime { get; private set; }


        public static float DeltaTime { get; private set; }

        public static float UnscaledDeltaTime { get; private set; }

        public static float PhysicalDeltaTime { get; private set; }


        public static float UnityTime => UnityEngine.Time.time;

        public static float UnityRealTime => UnityEngine.Time.realtimeSinceStartup;

        public float TimeMultipler
        {
            get => _multipler;
            set => _multipler = value;
        }

        #endregion

        protected void Start()
        {
            UnscaledTime = UnityRealTime;
            _lastRealTime = UnscaledTime;
            _lastPhysicsTime = UnityRealTime;

            DeltaTime = 0;
            Time = 0;
            UnscaledDeltaTime = 0;
            PhysicalTime = 0;
        }

        protected void Update()
        {
#if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isPaused)
            {
                UnscaledDeltaTime = 0;
                DeltaTime = 0;
                _lastRealTime = UnityRealTime;
                return;
            }
#endif

            UnscaledDeltaTime = UnityRealTime - _lastRealTime;
            UnscaledTime += UnscaledDeltaTime;

            DeltaTime = UnscaledDeltaTime * _multipler;
            Time += DeltaTime;

            _lastRealTime = UnityRealTime;
        }

        private void FixedUpdate()
        {
#if UNITY_EDITOR
            if (UnityEditor.EditorApplication.isPaused)
            {
                PhysicalDeltaTime = 0;
                _lastPhysicsTime = UnityRealTime;
                return;
            }
#endif
            PhysicalDeltaTime = (UnityRealTime - _lastPhysicsTime) * _multipler;
            PhysicalTime += PhysicalDeltaTime;

            _lastPhysicsTime = UnityRealTime;
        }

        #region TimeControl

#if CRAZY_TWEEN_INCLUDED
        public void ScaleTo(float to, float time, AnimationCurve curve = null)
        {
            _timeScaleTween.StopActive();

            var refTarget = new CrazyEaseRef<float>(gameObject, () => _multipler, f => _multipler = f,
                (t, from, end) => Mathf.Lerp(from, end, t));
            _timeScaleTween = new RefTween<float>(refTarget, _multipler, to, time)
                .SetAnimationCurve(curve)
                .SetOnComplete(OnScaleEnd)
                .SetIgnoreTimeScale(true);
            _timeScaleTween.Start();
        }

        private void OnScaleEnd()
        {
            _timeScaleTween = null;
        }
#endif

        public void ForceScale(float to)
        {
#if CRAZY_TWEEN_INCLUDED
            _timeScaleTween.StopActive();
#endif
            _multipler = to;
        }

        #endregion
    }
}
